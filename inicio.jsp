<%-- 
    Document   : inicio
    Created on : 08-jun-2020, 22:49:28
    Author     : PC
--%>

<%@page import="java.sql.*"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>
    </head>
    <body  >
         <%
        Class.forName("com.mysql.cj.jdbc.Driver");
        Connection conex = (Connection)DriverManager.getConnection("jdbc:mysql://127.0.0.1/proy102?useUnicode=true&useJDBCCompliantTimezoneShift=true&useLegacyDatetimeCode=false&serverTimezone=UTC&useSSL=false","root","31607");
        Statement sql = conex.createStatement();
        String qry = "select * from instrumentos";
        ResultSet data = sql.executeQuery(qry);
        %>
         <div style="text-align:center;">
            
            <h1>Instrumentos Musicales</h1>
            <hr>
            <a href="agregar.jsp"  >agregar</a>
            <br>
             <h2 style="text-align:center;">Registros</h2>
             <table border ="1" style="text-align:center;"  align="center">
                <tr>
                    <th>ID</th>
                    <th>Nombre</th>
                    <th>Tipo</th>
                    <th>Descripcion</th>
                    <th>Modificaciones</th>            
                </tr>
                <%
                   while(data.next()){
                       if(data.getInt("eliminar")!=1){
                %>
                <tr style="visibility:collapse" >
                    <%               
                 }
                %>
                    <td> <% out.print(data.getInt("id"));%></td>
                    <td> <% out.print(data.getString("nombre"));%></td>
                    <td> <% out.print(data.getString("tipo"));%></td>
                    <td> <% out.print(data.getString("descripcion"));%></td> 
                    <td class="text-center">
                        <a href="editar.jsp?id=<%= data.getInt("id")%>">editar</a>
                         <a href="eliminar.jsp?id=<%= data.getInt("id")%>">eliminar</a>
                    </td>
                </tr>
                <%               
                 }
                %>
            </table>  
        </div>
    </body>
</html>
